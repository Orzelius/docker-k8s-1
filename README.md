# learn docker and k8s

 1. Build docker image from Dockerfile given in this project (`docker build`)
 1. Run docker image build from this project (`docker run`)
 1. Add [docker-compose](https://docs.docker.com/compose/) with web folder mounted to container for development
 1. Push docker image build from this project to GitLab docker registry (`docker push`)
 1. Build docker image in GitLab CI ([docs](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html))
 1. Add Kubernetes deployment for your docker image ([example](https://gitlab.com/eritikass/tpt-dockertest/-/tree/master/kubernetes))
      - Expose using Ingress (so it would have human readable dns hostname)
 1. Make GitLab CI to update image in Kubernetes
